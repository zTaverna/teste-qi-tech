"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from django.contrib import admin
from rest_framework import routers
from .views import ProductViewSet, ProductRandomView
from rest_framework_swagger.views import get_swagger_view


swagger_view = get_swagger_view(title='API QI-Tech')

router = routers.DefaultRouter()
router.register(r'products', ProductViewSet)

urlpatterns = [
    path('product-random/', ProductRandomView.as_view()),
    path('swagger/', swagger_view),
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
]
