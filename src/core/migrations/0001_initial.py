# Generated by Django 3.2.12 on 2022-03-09 22:49

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ProductModel',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True, default=None, null=True)),
                ('link', models.CharField(blank=True, default=None, max_length=255, null=True)),
                ('photo', models.CharField(blank=True, default=None, max_length=255, null=True)),
                ('acquired', models.BooleanField(default=False)),
            ],
        ),
    ]
