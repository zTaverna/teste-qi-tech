from django import forms
from .models import ProductModel
 
# creating a form
class ProductForm(forms.ModelForm):
 
    # create meta class
    class Meta:
        # specify model to be used
        model = ProductModel
 
        # specify fields to be used
        fields = [
            "title",
            "description",
        ]