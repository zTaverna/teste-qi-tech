from core.models import ProductModel
from core.serializers import ProductSerializer
from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response


class ProductViewSet(viewsets.ModelViewSet):
    queryset = ProductModel.objects.all()
    serializer_class = ProductSerializer


class ProductRandomView(APIView):
    def get(self, request, format=None):
        product = ProductModel.objects.order_by('?')[0]
        serializer = ProductSerializer(product)
        
        return Response(serializer.data)
