from django.db import models

class ProductModel(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(default=None, blank=True, null=True)
    link = models.CharField(max_length=255, default=None, blank=True, null=True)
    photo = models.CharField(max_length=255, default=None, blank=True, null=True)
    acquired = models.BooleanField(default=False)

    def __str__(self):
        return self.title
