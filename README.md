# API QI-Tech

Esta API contempla o teste realizado para o perfil de Desenvolvedor Júnior.

## Configuração Inicial

#### Requisitos

- Docker e Docker-compose instalados

#### Instalação

Para realizar a instalação do projeto é necessário que o usuário digite os comandos abaixo em seu terminal:

```bash
docker-compose run web python manage.py makemigrations core
docker-compose run web python manage.py migrate core
```

Ao digitar os comandos acima, você irá realizar as migrações de Banco de Dados.

#### Inicialização

Para inicializar a aplicação, basta executar o comando `docker-compose up`.

Um servidor web será iniciado e acessível através da URL: `http://localhost:8000`.

## Documentação Técnica

A aplicação conta com uma interface do Swagger para que os endpoints possam ser testados.

O Swagger é acessado através da URL `http://localhost:8000/swagger/`.

### Listar todos os produtos

`GET /products/`

#### Resposta

```json
[
    {
        "title": "string",
        "description": "string",
        "link": "string",
        "photo": "string",
        "acquired": "boolean"
    },
    { ... }
]
```

### Obter informações de um produto pelo id

`GET /products/{id}/`

#### Resposta

```json
{
  "title": "string",
  "description": "string",
  "link": "string",
  "photo": "string",
  "acquired": "boolean"
}
```

### Adicionar um novo produto

`POST /products/`

#### Corpo da requisição

```json
{
  "title": "string", // campo obrigatório
  "description": "string", // opcional
  "link": "string", // opcional
  "photo": "string", // opcional
  "acquired": "boolean" // opcional, padrão: false
}
```

### Editar um produto pelo id

`PUT /products/{id}/`

#### Corpo da requisição

```json
{
  "title": "string",
  "description": "string",
  "link": "string",
  "photo": "string",
  "acquired": "boolean"
}
```

### Deletar um produto pelo id

`DELETE /products/{id}`

### Obter informações de um produto randômico

`GET /product-random/`

#### Resposta

```json
{
  "title": "string",
  "description": "string",
  "link": "string",
  "photo": "string",
  "acquired": "boolean"
}
```
